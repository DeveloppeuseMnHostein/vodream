<?php

////////////////////////////// Message /////////////////////////////////
class BDD_VOdream
{
    // Attributs
    protected $IsConnected;
    public $ErrCode;
    public $ErrDesc;
    public $Database;



    protected function Connect(bool $Order): bool
    {
        if ($Order) {
            try {
                $typeServeurBdd    = 'mysql';
                $nomBdd            = 'vodream';
                $adresseServeurBdd = 'localhost';
                $userName          = 'root';
                $userPassword      = '';
                $this->Database    = new \PDO(
                    $typeServeurBdd . ':dbname=' . $nomBdd . ';host=' . $adresseServeurBdd,
                    $userName,
                    $userPassword
                );
                $mirec = "set names utf8";
                $MyReq   = $this->Database->prepare($mirec);

                $MyReq->execute();

                return true;
            } catch (PDOException $e) {
                echo $e;
                return false;
            }
        }
    }

    public function getCat()
    {
        if ($this->Database != null) {
            try {

                $MyReq   = $this->Database->prepare("SELECT catégorie.ID,catégorie.Nom
                FROM catégorie ");

                if ($MyReq->execute()) {
                    $MyArray = $MyReq->fetchAll(PDO::FETCH_ASSOC);
                    return $MyArray;
                } else {
                    print 'On a un problème' . PHP_EOL;
                    return false;
                }
            } catch (PDOException $e) {
                var_dump($e);
                return false;
            }
        }
    }

    public function getProduits()
    {
        if ($this->Database != null) {
            try {

                $MyReq   = $this->Database->prepare("SELECT Titre,Date_de_sortie,Synopsis,vidéo
                FROM vodream.contenue");

                if ($MyReq->execute()) {
                    $MyArray = $MyReq->fetchAll(PDO::FETCH_ASSOC);
                    return $MyArray;
                } else {
                    print 'On a un pb' . PHP_EOL;
                    return false;
                }
            } catch (PDOException $e) {
                var_dump($e);
                return false;
            }
        }
    }
}
